/******************************************************************************************************************
* File:MiddleFilter.java
* Course: 17655
* Project: Assignment 1
* Copyright: Copyright (c) 2003 Carnegie Mellon University
* Versions:
*	1.0 November 2008 - Sample Pipe and Filter code (ajl).
*
* Description:
*
* This class serves as an example for how to use the FilterRemplate to create a standard filter. This particular
* example is a simple "pass-through" filter that reads data from the filter's input port and writes data out the
* filter's output port.
*
* Parameters: 		None
*
* Internal Methods: None
*
******************************************************************************************************************/
import java.util.*;
import java.text.SimpleDateFormat;

public class MiddleFilterMod extends FilterFramework
{
	public void run()
    {


		int bytesread = 0;					// Number of bytes read from the input file.
		int byteswritten = 0;				// Number of bytes written to the stream.
		byte databyte, databyteI, databyteM = 0;					// The byte of data read from the file

		int IdLength = 4;
		int MeasurementLength = 8;
		int id;
		int i;
		long measurement = 0;
		String[] ids = {"time", "velo", "pressure", "temp", "alt", "Attitude"};
		Calendar TimeStamp = Calendar.getInstance();
		SimpleDateFormat TimeStampFormat = new SimpleDateFormat("yyyy MM dd::hh:mm:ss:SSS");

		// Next we write a message to the terminal to let the world know we are alive...

		System.out.print( "\n" + this.getName() + "::Middle Reading ");

		while (true)
		{
			/*************************************************************
			*	Here we read a byte and write a byte
			*************************************************************/

			try
			{
				/*
				measurement = 0;

				for (i=0; i<MeasurementLength; i++ )
				{
					databyteM = ReadFilterInputPort();
					measurement = measurement | (databyteM & 0xFF);	// We append the byte on to measurement...

					if (i != MeasurementLength-1)					// If this is not the last byte, then slide the
					{												// previously appended byte to the left by one byte
						measurement = measurement << 8;				// to make room for the next byte we append to the
																	// measurement
					} // if

					bytesread++;									// Increment the byte count

				} // if

				if ( id == 0 )
				{
					TimeStamp.setTimeInMillis(measurement);

				} // if

				*/

				//std code
				databyte = ReadFilterInputPort();

				//read id
				id = 0;
				for (i=0; i<IdLength; i++ )
				{
					databyteI = ReadFilterInputPort();	// This is where we read the byte from the stream...

					id = id | (databyteI & 0xFF);		// We append the byte on to ID...

					if (i != IdLength-1)				// If this is not the last byte, then slide the
					{									// previously appended byte to the left by one byte
						id = id << 8;					// to make room for the next byte we append to the ID

					} // if

					//bytesread++;						// Increment the byte count

				}

				//std code
				bytesread++;

				//write data to output
				WriteFilterOutputPort(databyte);
				byteswritten++;

				String idName = "";
				if (id > 0 && id < ids.length) 
					idName = ids[id];
				else 
					idName = Integer.toString(id);
				System.out.println(" > " + TimeStampFormat.format(TimeStamp.getTime()) + " ID = " + idName + " " + Double.longBitsToDouble(measurement) );

			} // try

			catch (EndOfStreamException e)
			{
				ClosePorts();
				System.out.println( "\n" + this.getName() + "::Middle Exiting ( bytes read: " + bytesread + " bytes written: " + byteswritten + ")" );
				break;

			} // catch

		} // while

   } // run

} // MiddleFilter